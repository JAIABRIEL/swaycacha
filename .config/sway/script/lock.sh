#!/bin/bash
#
swayidle \
    timeout 10 'swaymsg "output * dpms off"' \
    resume 'swaymsg "output * dpms on"' &

keepassxc --lock

swaylock -c 282a36
kill %%
