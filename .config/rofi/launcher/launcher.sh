#!/usr/bin/env bash

export QT_QPA_PLATFORMTHEME=gtk3

rofi \
	-show drun \
	-modi run,drun,ssh \
	-scroll-method 0 \
	-drun-match-fields all \
	-drun-display-format "{name}" \
	-no-drun-show-actions \
	-terminal alacritty \
	-kb-cancel Escape \
	-theme "$HOME"/.config/rofi/launcher/launcher.rasi \
	-icon-theme "Tela-circle-dracula-dark"
