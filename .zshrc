### Added by Zinit's installer
ZINIT_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}/zinit/zinit.git"
[ ! -d $ZINIT_HOME ] && mkdir -p "$(dirname $ZINIT_HOME)"
[ ! -d $ZINIT_HOME/.git ] && git clone https://github.com/zdharma-continuum/zinit.git "$ZINIT_HOME"
source "${ZINIT_HOME}/zinit.zsh"

export SWAYCACHA_CONFIG="${HOME}/.config/swaycacha"

#source "$HOME/.zinit/bin/zinit.zsh"
#autoload -Uz _zinit
#(( ${+_comps} )) && _comps[zinit]=_zinit


#zinit ice depth"1"

HISTFILE=~/.zsh_history
HISTSIZE=30000
SAVEHIST=30000
setopt SHARE_HISTORY

# always return full history
alias history="history 1"
alias shs="history | grep -i"

alias "~"="cd ~"
alias ".."="cd .."

zinit light axieax/zsh-starship

zinit ice wait lucid

zinit light agkozak/zsh-z

zmodload zsh/complist # for 'shift + tab'

zinit ice as"program" pick"bin/git-dsf"
zinit light zdharma-continuum/zsh-diff-so-fancy
zinit light zsh-users/zsh-syntax-highlighting

zinit ice depth=1
zinit light jeffreytse/zsh-vi-mode

zinit light junegunn/fzf
zinit light Aloxaf/fzf-tab
zinit light zsh-users/zsh-autosuggestions
zinit light zsh-users/zsh-completions


# Git alias snippet from ohmyzsh
zinit snippet https://github.com/ohmyzsh/ohmyzsh/blob/master/plugins/git/git.plugin.zsh



#export ZSH="${HOME}/.oh-my-zsh"

#eval "$(starship init zsh)"

# Oh-My-Zsh

plugins=(
  git
  docker
  docker-compose
  gitfast
  git-flow-avh
  vi-mode
  colorize
  extract
  colored-man-pages
  jsontools
  dnf
)


#source $HOME/.oh-my-zsh/oh-my-zsh.sh
source $SWAYCACHA_CONFIG/zsh/alias.zsh

# change ls to lsd if lsd is installed
if hash lsd 2>/dev/null; then
	alias "ls"="lsd"
fi

# ===
# Android / LineageOS
# ===
export USE_CCACHE=1
export CCACHE_EXEC=/usr/bin/ccache

# add Android SDK platform tools to path
if [ -d "$HOME/Dev/android_sdk/platform-tools" ] ; then
    PATH="$HOME/Dev/android_sdk/platform-tools:$PATH"
fi

# ===
# Golang
# ===

export GOPATH=$HOME/.go
export PATH=$PATH:$GOPATH/bin
export GOPROXY="direct"
export GO111MODULE=on

# ===
# Rust
# ===

export PATH=$PATH:~/.cargo/bin

# ===
# Flutter
# ===

export PATH="$PATH:$HOME/Dev/flutter/bin"
export PATH="$PATH:$HOME/Dev/android_sdk/cmdline-tools/latest/bin"
export ANDROID_HOME="$HOME/Dev/android_sdk"

# ===
# Git
# ===
alias -g glg='git log --stat --show-signature'
# ===
# General
# ===
export EDITOR=nvim

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/Dev/bin" ] ; then
    PATH="$HOME/Dev/bin:$PATH"
fi

autoload compinit; compinit
