# Fedora

## Enable repositories

`dnf copr enable erikreider/SwayNotificationCenter`

`dnf copr enable atim/starship`

`dnf config-manager --add-repo https://download.opensuse.org/repositories/home:TheLocehiliosan:yadm/Fedora_37/home:TheLocehiliosan:yadm.repo`

## Install packages

```
dnf install yadm SwayNotificationCenter starship sway swaylock waybar wofi ydotool wlroots \
	qt5-qtwayland qt6-qtwayland thunar bpytop nwg-launchers alacritty neovim git \
	grim swappy slurp pavucontrol zsh brightnessctl gtk-layer-shell-devel rustup neofetch wlsunset python3-pip

```

Tiling in Sway like BSPWM.
```
sudo pip install autotiling
```

## Build Rust packages

Set Rust to nightly

```
rustup-init
rustup default nightly
```

Clone [eww](https://github.com/elkowar/eww) by running ```git clone https://github.com/elkowar/eww.git```.

Install eww by running

```
cargo install --path eww/crates/eww
```


# General

## Install GTK and icon theme

```
git clone https://github.com/vinceliuice/Colloid-gtk-theme.git && ./Colloid-gtk-theme/install.sh --libadwaita --theme purple --color dark --size compact --tweaks dracula
```

```
git clone https://github.com/vinceliuice/Colloid-icon-theme.git && ./Colloid-icon-theme/install.sh --scheme dracula --theme purple
```

```
git clone https://github.com/vinceliuice/Tela-circle-icon-theme.git && ./Tela-circle-icon-theme/install.sh dracula 
```

```
wget -cO- https://github.com/phisch/phinger-cursors/releases/latest/download/phinger-cursors-variants.tar.bz2 | tar xfj - -C ~/.local/share/icons
```

## Map `LSHIFT` to `CAPSLOCK`

Edit file `/etc/udev/hwdb.d/replace_caps.hwdb` and add

```
evdev:input:b0011v0001p0001*
 KEYBOARD_KEY_3a=leftshift
```

Then run `systemd-hwdb update` as root user and reboot the device.


More information can be found [here](https://github.com/gkovacs/udev-key-remapping/blob/master/61-meta-key.hwdb).
